#ifndef BSNode_H
#define BSNode_H

#include <string>
#include <iostream>
#include <exception>


using namespace std;

template<typename T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode<T>& other);

	virtual ~BSNode();
	
	virtual BSNode<T>* insert(T value);
	BSNode<T>& operator=(const BSNode<T>& other);

	bool isLeaf() const;
	T getData() const;
	BSNode<T>* getLeft() const;
	BSNode<T>* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode<T>& root) const;

	void printNodes() const;

	void printTree() const;
	BSNode<T>* insert(BSNode<T>* node);

protected:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;

	int _count;
};

///FUNCTIONS\\\

/*
constructor for BSNode class
input: the data object of the new node
none:outout
*/
template<typename T>
BSNode<T>::BSNode(T data) : _left(nullptr), _right(nullptr), _data(data), _count(1)
{
}

/*
constructor for the BSNode class
input: BSNode pointer to deep copy data from
none:output
*/
template<typename T>
BSNode<T>::BSNode(const BSNode<T>& other)
{
	*this = other;
}

/*
destructor for the BSNode class
input:none
none:output
*/
template<typename T>
BSNode<T>::~BSNode()
{
	if (this->_left)
	{
		delete this->_left;
	}
	if (this->_right)
	{
		delete this->_right;
	}
}

/*
inserts a new valiue to the binary search tree
input: the new data object
output:none
*/
template<typename T>
BSNode<T>* BSNode<T>::insert(T value)
{
	if (value < _data)
	{
		if (_left)
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode<T>(value);
		}
	}
	else if (value > _data)
	{
		if (_right)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode<T>(value);
		}
	}
	else
	{
		_count++;
	}
	return this;
}

/*
deep copy for the class BSNode using operaotr overloading
input: the BSNode object to copy data from
output: refrence to the BSNode that was changed
*/
template<typename T>
BSNode<T>& BSNode<T>::operator=(const BSNode<T>& other)
{
	try
	{
		if (_left)
		{
			delete _left;
		}
		if (_right)
		{
			delete _right;
		}
	}
	catch (exception e)
	{
		cout << endl << e.what() << endl;
	}
	_data = other._data;
	_left = 0;
	_right = 0;

	//deep recursive copy
	if (other._left)
	{
		_left = new BSNode<T>(*other._left);
	}

	if (other._right)
	{
		_right = new BSNode<T>(*other._right);
	}
	_count = other._count;
	return *this;
}

/*
cheks if the node is a leaf
input:none
output: true/false
*/
template<typename T>
bool BSNode<T>::isLeaf() const
{
	return _left == nullptr && _right == nullptr;
}

/*
getter for the data of the node
input:none
output: the data object as const
*/
template<typename T>
T BSNode<T>::getData() const
{
	return _data;
}

/*
getter for pointer to the left node
input:none
output: pointer to the left node as const
*/
template<typename T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return _left;
}

/*
getter for pointer to the right node
input:none
output: pointer to the right node as const
*/
template<typename T>
BSNode<T>* BSNode<T>::getRight() const
{
	return _right;
}

/*
checks if a given valiue exists n the binary search tree
input: the object to search
output: true/false
*/
template<typename T>
bool BSNode<T>::search(T val) const
{
	bool returner = false;
	if (val < _data)
	{
		if (this->_left)
		{
			returner = this->_left->search(val);
		}
	}
	else if (val > _data)
	{
		if (this->_right)
		{
			returner = this->_right->search(val);
		}
	}
	else
	{
		returner = true;
	}
	return returner;
}

/*
returns the height of the tree
input:none
output: the height
*/
template<typename T>
int BSNode<T>::getHeight() const
{
	int heightRight = 0;
	int heightLeft = 0;
	if (!isLeaf())
	{
		if (_left)
		{

			heightLeft += _left->getHeight() + 1;
		}
		if (_right)
		{
			heightRight += _right->getHeight() + 1;
		}
	}
	else
	{
		return 1;
	}
	return heightRight < heightLeft ? heightLeft : heightRight;
}

/*
gets the depth of the node from a given root
input: refrence to the root as constant
output: the depth OR -1 if the node is not existing on that root
*/
template<typename T>
int BSNode<T>::getDepth(const BSNode<T>& root) const
{
	if (this->_data < root._data)
	{
		if (root._left)
		{
			return getDepth(*(root._left)) + 1;
		}
		else
		{
			return -1;
		}
	}
	else if (this->_data > root._data)
	{
		if (root._right)
		{
			return getDepth(*(root._right)) + 1;
		}
		else
		{
			return -1;
		}
	}
	else
	{
		return 0;
	}
}

/*
prints the data in the nodes of the tree
input:none
outout:none
*/
template<typename T>
void BSNode<T>::printNodes() const
{
	if (_left)
	{
		_left->printNodes();
	}
	std::cout << _data << " , " << _count << endl;
	if (_right)
	{
		_right->printNodes();
	}
}

/*
prints the tree in the format root ( leftNode | rightNode )
input:none
output:none
*/
template<typename T>
void BSNode<T>::printTree() const
{
	std::cout << _data << " (";
	if (_left)
	{
		_left->printTree();
	}
	else
	{
		std::cout << "# ";
	}
	std::cout << "| ";
	if (_right)
	{
		_right->printTree();
	}
	else
	{
		std::cout << "# ";
	}
	std::cout << ") ";
}

/*
inserts pointer to BSNode<T> object to the tree
input: pointer to the object to insert;
output: the root
*/
template<typename T>
BSNode<T>* BSNode<T>::insert(BSNode<T>* node)
{
	if (node->_data < _data)
	{
		if (_left)
		{
			_left->insert(node);
		}
		else
		{
			_left = node;
		}
	}
	else if (node->_data > _data)
	{
		if (_right)
		{
			_right->insert(node);
		}
		else
		{
			_right = node;
		}
	}
	return this;
}

#endif