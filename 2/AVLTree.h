///THIS CLASS IS NOT FINISHED!!!\\\
\
\
//I would make an AVLTree class wich contains an BSTNode object as the tree, and not as here wich is inhariting from it.
#pragma once
#include "BSNode.h"
template<typename T>
class AVLTree :	public BSNode<T>
{
public:
	AVLTree(T data);
	AVLTree(BSNode<T> node);
	~AVLTree();

	AVLTree<T>* insert(T value);

	BSNode<T>* rotateRight();
	BSNode<T>* rotateLeft();
	
private:
	int getBalanceFactor();
};

/*
constructor for AVLTree<T> class with given data
input: the data
none:output
*/
template<typename T>
AVLTree<T>::AVLTree(T data) : BSNode<T>(data)
{
}

/*
constructor for AVLTree<T> class from given BSNode<T> object
input: the object
none:output
*/
template<typename T>
AVLTree<T>::AVLTree(BSNode<T> node) : BSNode<T>(node)
{
}

/*
disstructor for AVLTree<T> class 
input:none
none:output
*/
template<typename T>
AVLTree<T>::~AVLTree()
{
}

///UNFINISHED FUNCTION
/*
inserts a new value to the tree
input: the value
output: the pointer to the root of the tree
*/
template<typename T>
AVLTree<T>* AVLTree<T>::insert(T value)
{
	BSNode<T>::insert(value);
	if (getBalanceFactor() == -2)
	{
		//the same algorithm for RR and RL for what I saw, but wasn't so sure about it, and hadn't had the time to check it deeply
		//RR
		if (this2->getRight()->getData() < value)
		{
			this->_right = rotateLeft();
		}
		//RL
		else
		{
			this->_right = rotateLeft();
		}
		///cannot convert from BSNode<T>* to AVLTree<T>* so used this as a return for debugging and checking the functions.   \
			this problem took a lot of my time, and I hadn't found solution.	\
			this change destroyed some of the functionalities of the other function that without activating the rotate state would work fine
//		this = this->getRight();
		return this;
	}
	else if (getBalanceFactor() == 2)
	{
		//the same algorithm for LR and LL for what I saw, but wasn't so sure about it, and hadn't had the time to check it deeply
		//LL
		if (this2->getLeft()->getData() < value)
		{
			this->_left = rotateRight();
		}
		//LR
		else
		{
			this->_left = rotateRight();
		}
		///SAME AS ABOVE
		//this = this->getLeft();
		return this;
	}
	return this;
}

/*
calculates the balance factor of the root
input:none
output: the factor (+ -> left side , - -> right side)
*/
template<typename T>
int AVLTree<T>::getBalanceFactor()
{
	int rightH = 0, leftH = 0;
	//if left son exists
	if (this->_left)
	{
		leftH = this->_left->getHeight();
	}
	//if right son exists
	if (this->_right)
	{
		rightH = this->_right->getHeight();
	}
	return leftH - rightH;
}

/*
rotates the root to the right
input:none
output: pointer to the new root 
*/
template<typename T>
BSNode<T>* AVLTree<T>::rotateRight()
{
	BSNode<T>* temp = this->getLeft();
	this->_left = NULL;
	BSNode<T>* this2 = this;
	temp->insert(this2);
	return temp;
}

/*
rotates the root to the left
input:none
output: pointer to the new root
*/
template<typename T>
BSNode<T>* AVLTree<T>::rotateLeft()
{
	BSNode<T>* temp = AVLTree<T>::getRight();
	this->_right = NULL;
	BSNode<T>* this2 = this;
	temp->insert(this2);
	return temp;
}
