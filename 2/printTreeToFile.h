//taken from the solutions for lesson 4

#pragma once

#include <string>
#include "BSNode.h"
#include <fstream>

using namespace std;

template<typename T>
void printTreeToFile(const BSNode<T>* bs, std::string output);

/*
prints the data from the tree into a file
input: pointer to the tree, output-stream to the file
output:none
*/
template<typename T>
void printData(const BSNode<T>* node, ofstream& myfile)
{
	if (node == NULL)
		myfile << "# ";
	else
	{
		myfile << node->getData() << " ";   // change to the name of function that return the data
		printData(node->getLeft(), myfile); // change to the name of function that return the left node
		printData(node->getRight(), myfile);// change to the name of function that return the right node
	}
}

/*
prints the tree into a file
input: pointer to the tree, the name of the file
output:none
*/
template<typename T>
void printTreeToFile(const BSNode<T>* bs, string output)
{
	ofstream myfile;
	myfile.open(output, ios::out);

	printData(bs, myfile);

	myfile.close();
}