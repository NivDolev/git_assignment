#include <iostream>
#include <string>

#define A_BIGGER_B -1
#define A_EQUAL_B 0
#define A_SMALLER_B 1

using namespace std;

/*
compares 2 objects of the same type
	|object must have <,== operators
input: two arguments of the same objects
output: 1 - a < b OR 0 a == b OR -1 a > b
*/
template<typename T>
int compare(T a, T b)
{
	if (a < b)
	{
		return A_SMALLER_B;
	}
	else if (a == b)
	{
		return A_EQUAL_B;
	}
	else
	{
		return A_BIGGER_B;
	}
}


/*
sorts a given array of objects via bubble sort
	|object must have < operator
input: array of objects, the size of the array
output:none
*/
template<typename T>
void bubbleSort(T* arr, int size)
{
	T min;
	int j, i;
	for (i = 0; i < size; i++)
	{
		min = arr[i];
		int small = i;
		for (j = i; j < size; j++)
		{
			if (arr[j] < min)
			{
				min = arr[j];
				small = j;
			}
		}
		arr[small] = arr[i];
		arr[i] = min;
	}
}

/*
prints a given array of objects
input: array of objects, the size of the array
output:none
*/
template<typename T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

void main()
{
	//variables for compare
	int a = 1, b = 2;
	char* c = "a",* d = "a";
	float e = 3, f = 1;
	string g("a"), h("b");
	//arrays for bubbleSort and printArray
	int arr[10] = { 1, 7 , 3 , 6 , 3, 7, 8, 6, 0, 3 };
	char str[11] = "anfrkdlsko";
	float arr2[10] = { 4, 7.4 , 3.2 , 6 , 3.09, 7, 8.9, 6.5, 0.2, 3 };
	string s[10] = { string("s") ,  string("a"), string("ab"), string("v"), string("b"), string("3"), string("cb"), string("aa"), string("a"), string("d") };
	//compare
	cout << compare(a, b) << endl;
	cout << compare(c, d) << endl;
	cout << compare(e, f) << endl;
	cout << compare(g, h) << endl;
	//bubbleSort
	bubbleSort(arr, 10);
	bubbleSort(str, 10);
	bubbleSort(arr2, 10);
	bubbleSort(s, 10);
	//printArray
	printArray(arr, 10);
	printArray(str, 10);
	printArray(arr2, 10);
	printArray(s, 10);

	system("pause");
}
